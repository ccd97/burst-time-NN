import numpy as np


class NeuralNet3L:

    def __init__(self, hidn1=100, hidn2=20, lr=0.00001, momentum=0.9, activations=['tanh', 'tanh', 'sigmoid']):
        self.n_input = None
        self.n_hidden1 = hidn1
        self.n_hidden2 = hidn2
        self.n_output = None
        self.learning_rate = lr
        self.momentum = momentum

        self.WL1 = None
        self.WL2 = None
        self.WL3 = None

        try:
            activation_f = {
                'identity': lambda x: x,
                'sigmoid': lambda x: 1.0 / (1.0 + np.exp(-x)),
                'tanh': lambda x: np.tanh(x),
                'relu': lambda x: x * (x > 0),
            }

            activation_f_prime = {
                'identity': lambda x: 1,
                'sigmoid': lambda x: x * (1.0 - x),
                'tanh': lambda x: 1 - x**2,
                'relu': lambda x: 1.0 * (x > 0),
            }

            self.act_f1 = activation_f[activations[0]]
            self.act_f2 = activation_f[activations[1]]
            self.act_f3 = activation_f[activations[2]]

            self.act_f1_prime = activation_f_prime[activations[0]]
            self.act_f2_prime = activation_f_prime[activations[1]]
            self.act_f3_prime = activation_f_prime[activations[2]]

        except KeyError:
            print("Error: Invalid activation functions")

    # Training Function
    def __train(self, X, Y):
        if X.ndim < 2:
            X = np.expand_dims(X, axis=0)
        if Y.ndim < 2:
            Y = np.expand_dims(Y, axis=0)

        # forward prop
        h1_inter = np.dot(X, self.WL1['weight'])
        h1_result = self.act_f1(h1_inter)
        h2_inter = np.dot(h1_result, self.WL2['weight'])
        h2_result = self.act_f2(h2_inter)
        o_inter = np.dot(h2_result, self.WL3['weight'])
        o_result = self.act_f3(o_inter)

        error = np.mean(0.5 * np.square(o_result - Y))

        # back prop
        del_WL3 = -np.multiply(Y - o_result, self.act_f3_prime(o_result))
        grad_WL3 = np.dot(h2_result.T, del_WL3)

        del_WL2 = np.dot(del_WL3, self.WL3['weight'].T) * self.act_f2_prime(h2_result)
        grad_WL2 = np.dot(h1_result.T, del_WL2)

        del_WL1 = np.dot(del_WL2, self.WL2['weight'].T) * self.act_f1_prime(h1_result)
        grad_WL1 = np.dot(X.T, del_WL1)

        return error, grad_WL1, grad_WL2, grad_WL3

    def fit(self, X, Y, epoch=10, batch_size=1000):
        if self.n_input is not None and self.n_output is not None:
            raise RuntimeError("Model already trained. Use fit1 to train just one field")

        if Y.ndim < 2:
            Y = Y.reshape(-1, 1)

        self.n_input = X.shape[1]
        self.n_output = Y.shape[1]

        self.WL1 = {
            'weight': np.random.normal(scale=0.1, size=[self.n_input, self.n_hidden1])
        }

        self.WL2 = {
            'weight': np.random.normal(scale=0.1, size=[self.n_hidden1, self.n_hidden2])
        }

        self.WL3 = {
            'weight': np.random.normal(scale=0.1, size=[self.n_hidden2, self.n_output])
        }

        tr_loss = []

        n_batch = X.shape[0] // batch_size

        # min-batch epoch training
        for e in range(epoch):

            for i in range(n_batch):
                x = X[i*batch_size:(i+1)*batch_size]
                y = Y[i*batch_size:(i+1)*batch_size]
                loss, grad_WL1, grad_WL2, grad_WL3 = self.__train(x, y)

                # Adjust Weights
                self.WL1['weight'] -= self.learning_rate * grad_WL1 + self.momentum * grad_WL1
                self.WL2['weight'] -= self.learning_rate * grad_WL2 + self.momentum * grad_WL2
                self.WL3['weight'] -= self.learning_rate * grad_WL3 + self.momentum * grad_WL3

                tr_loss.append(loss)

        return tr_loss

    def fit1(self, X, Y):
        if self.WL1 is None or self.WL2 is None or self.WL3 is None:
            raise RuntimeError("Fit the model with fit function first")

        loss, grad_WL1, grad_WL2, grad_WL3 = self.__train(X, Y)

        self.WL1['weight'] -= self.learning_rate * grad_WL1 + self.momentum * grad_WL1
        self.WL2['weight'] -= self.learning_rate * grad_WL2 + self.momentum * grad_WL2
        self.WL3['weight'] -= self.learning_rate * grad_WL3 + self.momentum * grad_WL3

        return loss

    def predict(self, X):
        if self.WL1 is None or self.WL2 is None or self.WL3 is None:
            raise RuntimeError("Fit the model first to predict")

        if X.ndim < 2:
            X = np.expand_dims(X, axis=0)

        h1_inter = np.dot(X, self.WL1['weight'])
        h1_result = self.act_f1(h1_inter)
        h2_inter = np.dot(h1_result, self.WL2['weight'])
        h2_result = self.act_f2(h2_inter)
        o_inter = np.dot(h2_result, self.WL3['weight'])
        pred = self.act_f3(o_inter)

        return pred
