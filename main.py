from utils import InputData
from kmodel import NeuralNet3L
from scheduler import SJF

from utils import InputData
from keras.models import Sequential
from keras.layers import Dense

import random
import pandas as pd
import matplotlib.pyplot as plt

# Load Training Data
inp_data = InputData("data/anon_jobs.db3")
train_data = inp_data.get_data()

# Plot Some Paramaters
fig = plt.figure(figsize=(15, 10))
fig.subplots_adjust(hspace=0.25)

ax = fig.add_subplot(2,2,1)
plt.xlabel('Process')
plt.ylabel('Normalized Time')
plt.title('Submit Time')
_ = ax.plot(train_data['input'][:,0])

ax = fig.add_subplot(2,2,2)
plt.xlabel('Process')
plt.ylabel('Normalized Time')
plt.title('Wait Time')
_ = ax.plot(train_data['input'][:,1])

ax = fig.add_subplot(2,2,3)
plt.xlabel('Process')
plt.ylabel('Normalized Time')
plt.title('Used CPU Time')
_ = ax.plot(train_data['input'][:,2])

ax = fig.add_subplot(2,2,4)
plt.xlabel('Process')
plt.ylabel('Normalized Time')
plt.title('Burst Time')
_ = ax.plot(train_data['target'])

features = train_data['input']
target = train_data['target']

model = NeuralNet3L()
history = model.fit(features, target, epochs=10, batch_size=100)

# Plot Losses
fig = plt.figure(figsize=(15, 5))

ax = fig.add_subplot(1,2,1)
plt.xlabel('Epoch')
plt.ylabel('Loss')
plt.title('Training Loss')
_ = ax.plot(history.history['loss'])

ax = fig.add_subplot(1,2,2)
plt.xlabel('Epoch')
plt.ylabel('Loss')
plt.title('Validation Loss')
_ = ax.plot(history.history['val_loss'])

# Sample Test Data from validation set
test_data = {}
rs = random.randint(0, int(train_data['input'].shape[0] * 0.15))
test_data['input'] = train_data['input'][-rs:-rs+10]
test_data['target'] = train_data['target'][-rs:-rs+10]
test_data['normalizer'] = train_data['normalizer']

# test_data = inp_data.sample_data(size=10)

# Predict Burst time
pred = model.predict(test_data['input'])
real = test_data['target']

# Plot difference
fig = plt.figure(figsize=(7, 5))

ax = fig.add_subplot(1,1,1)
plt.xlabel('Test Sample')
plt.ylabel('Normalized Result')
plt.title('Predicted-Real Comparision')
rects1 = ax.bar([x for x in range(len(pred))], pred, width=0.40)
rects2 = ax.bar([x+0.40 for x in range(len(real))], real, width=0.40)
_ = ax.legend((rects1[0], rects2[0]), ('Predicted', 'Real'))

# Get start time and burts time from data
normalizer = test_data['normalizer']
start_time = inp_data.denormalize(test_data['input'][:,0], normalizer['SubmitTime']) 
burst_time = inp_data.denormalize(pred, normalizer['RunTime'])
start_time = [(x - test_data['normalizer']['SubmitTime']['min'])/1000 for x in start_time]
burst_time = [x/1000 if x > 10 else 0.01 for x in burst_time]

# schedule process
scheduler = SJF()
result = scheduler.schedule(start_time, burst_time)

print("Average Turnaround Time : " + str(result['average_turnaround_time']))
print("Average Waiting Time : " + str(result['average_waiting_time']))

q = result['queues']
process = []

for i in range(len(q)):
    for j in range(len(q[i])):
        process.append([i, q[i][j].id, q[i][j].at, q[i][j].bt, q[i][j].wt, q[i][j].tat])

pd.DataFrame(process, columns=['Queue', 'Id', 'Arival Time', 'Burst Time', 'Waiting Time', 'Turnaround Time'])
