class SJF:

	class ProcessData:

		def __init__(self, id, at, bt):
			self.id = id
			self.at = at
			self.bt = bt
			self.wt = 0 
			self.tat = 0

	def __init__(self):
		self.__clear_queues()

	def __clear_queues(self):
		self.sq = [[],[],[],[]]
		self.qe = []
		self.no = 0

	def __load_data(self, start_times, burst_times):
		if len(start_times) != len(burst_times):
			raise Exception("Invalid sets of time data")

		for i in range(len(start_times)):
			self.qe.append(self.ProcessData(i, start_times[i], burst_times[i]))

		self.qe = sorted(self.qe, key=lambda x:x.at)
		self.no = len(self.qe)

	def __schedule(self):
		i = 0; a = 0; b = 0; c = 0; d = 0
		cp = []
		while True:
	
			while len(self.qe) > 0:
				if self.qe[0].at <= i:
					cp.append(self.qe.pop(0))
				else:
					break

			self.cp = sorted(cp,key=lambda x:x.bt)

			if len(cp) > 0:
				if i == a and len(cp) > 0:
					cp[0].wt = i - cp[0].at
					cp[0].tat = cp[0].wt + cp[0].bt
					a += cp[0].bt
					self.sq[0].append(cp.pop(0))
				if i == b and len(cp) > 0:
					cp[0].wt = i - cp[0].at
					cp[0].tat = cp[0].wt + cp[0].bt
					b += cp[0].bt
					self.sq[1].append(cp.pop(0))
				if i == c and len(cp) > 0:
					cp[0].wt = i - cp[0].at
					cp[0].tat = cp[0].wt + cp[0].bt
					c += cp[0].bt
					self.sq[2].append(cp.pop(0))
				if i == d and len(cp) > 0:
					cp[0].wt = i - cp[0].at
					cp[0].tat = cp[0].wt + cp[0].bt
					d += cp[0].bt
					self.sq[3].append(cp.pop(0))
				i += 1
				a = i if a < i else a
				b = i if b < i else b
				c = i if c < i else c
				d = i if d < i else d
			else:
				i += 1; a = i; b = i; c = i; d = i
			
			if len(self.qe) == 0 and len(cp) == 0:
				break

	def __get_result(self):
		atrt = 0.0
		awt = 0.0
		for i in range(len(self.sq[0])):
			awt += self.sq[0][i].wt
			atrt += self.sq[0][i].tat
		for i in range(len(self.sq[1])):
			awt += self.sq[1][i].wt
			atrt += self.sq[1][i].tat
		for i in range(len(self.sq[2])):
			awt += self.sq[2][i].wt
			atrt += self.sq[2][i].tat
		for i in range(len(self.sq[3])):
			awt += self.sq[3][i].wt
			atrt += self.sq[3][i].tat			
		atrt /= self.no
		awt /= self.no

		return {
			"queues": self.sq,
			"average_turnaround_time": atrt,
			"average_waiting_time": awt,
		}


	def schedule(self, start_times, burst_times):
		self.__load_data(start_times, burst_times)
		self.__schedule()
		result = self.__get_result()
		self.__clear_queues()
		return result