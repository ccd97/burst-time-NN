from utils import InputData
from keras.models import Sequential
from keras.layers import Dense
from keras.callbacks import TensorBoard

class NeuralNet3L:

	def __init__(self, hidn1=100, hidn2=20, activations=['tanh', 'tanh', 'tanh']):
		self.n_hidden1 = hidn1
		self.n_hidden2 = hidn2
		self.activations = activations

	def fit(self, X, Y, epochs=10, batch_size=1000):
		self.model = Sequential()
		self.model.add(Dense(self.n_hidden1, input_shape=(X.shape[1],), activation=self.activations[0]))
		self.model.add(Dense(self.n_hidden2, activation=self.activations[1]))
		self.model.add(Dense(1, activation=self.activations[2]))
		self.model.compile(loss='mean_squared_error', optimizer='adam')
		return self.model.fit(X, Y, epochs=epochs, batch_size=batch_size, validation_split=0.15)

	def predict(self, X):
		return self.model.predict(X)
