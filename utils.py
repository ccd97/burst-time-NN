import numpy as np
import pandas as pd
import sqlite3
import os.path


class InputData:

    def __init__(self, db_filepath):
        self.db_filepath = db_filepath
        self.npy_filepath = db_filepath.split('.')[0]+".npy"
        self.data = None

        if not os.path.exists(db_filepath):
            raise FileNotFoundError(db_filepath + " not found")

        if os.path.exists(self.npy_filepath):
            self.data = self.__load_data_from_npy()

    def __get_rare_values(self, col, limit=100):
        ids, counts = np.unique(col, return_counts=True)
        id_rare = list(ids)
        for e, c in zip(ids, counts):
            if c > limit:
                id_rare.remove(e)

        return id_rare

    def __replace_with_one_hot_cols(self, table, colname):
        return table.join(pd.get_dummies(table[colname], prefix=colname)).drop(colname, axis=1)

    def __save_data_to_npy(self, table, norm_var):
        data_dict = {
            'input': table.drop('RunTime', axis=1),
            'target': table['RunTime'],
            'normalizer': norm_var
        }
        self.data = {
            'input': data_dict['input'].as_matrix(),
            'target': data_dict['target'].as_matrix(),
            'normalizer':  data_dict['normalizer']
        }
        np.save(self.npy_filepath, data_dict)

    def __load_data_from_npy(self):
        npy_data = np.load(self.npy_filepath)
        return {
            'input': npy_data[()]['input'].as_matrix(),
            'target': npy_data[()]['target'].as_matrix(),
            'normalizer':  npy_data[()]['normalizer']
        }

    def __shuffle_data(self):
        perm = np.random.permutation(len(self.data['input']))
        self.data['input'] = self.data['input'][perm]
        self.data['target'] = self.data['target'][perm]

    def __load_data_from_db_plus_perprocess(self):
        db = sqlite3.connect(self.db_filepath)
        jobs = pd.read_sql('Select * from Jobs', db)

        # just first 18 colms (excluding JobId)
        jobs = jobs[jobs.columns[1:18]]

        # Drop - ReqNProc ReqMemory LastRunSiteID
        jobs = jobs.drop(['NProc', 'ReqNProcs', 'ReqMemory', 'LastRunSiteID'], axis=1)

        # Remove invalid data
        jobs = jobs.replace(['-1', -1], np.NaN).dropna()

        # too many ExecutableID, UserID (remove if too small quantity)
        jobs = jobs.replace(self.__get_rare_values(jobs.ExecutableID), 'XR')
        jobs = jobs.replace(self.__get_rare_values(jobs.UserID), 'rare')

        # Normalize Columns
        norm_var = dict()
        norm_cl = ['WaitTime', 'SubmitTime', 'UsedCPUTime', 'UsedMemory', 'ReqTime', 'RunTime']
        for cl in norm_cl:
            norm_var[cl] = dict()
            norm_var[cl]['mean'] = jobs[cl].mean()
            norm_var[cl]['min'] = jobs[cl].min()
            norm_var[cl]['max'] = jobs[cl].max()
            jobs[cl] = (jobs[cl] - norm_var[cl]['mean']) / (norm_var[cl]['max'] - norm_var[cl]['min'])

        # One hot encoding
        for col in ['Status', 'PartitionID', 'OrigSiteID', 'GroupID', 'QueueID', 'ExecutableID', 'UserID']:
            jobs = self.__replace_with_one_hot_cols(jobs, col)

        self.__save_data_to_npy(jobs, norm_var)

    def get_data(self, shuffle=True):

        if self.data is None:
            self.__load_data_from_db_plus_perprocess()

        if shuffle:
            self.__shuffle_data()

        return self.data

    def sample_data(self, size=10):
        table = pd.DataFrame(self.data['input'])
        table['target'] = self.data['target']
        sample_table=  table.sample(n=size)

        return {
            'target': sample_table['target'].as_matrix(),
            'input': sample_table.drop('target', axis=1).as_matrix(),
            'normalizer':  self.data['normalizer'],
        }

    def denormalize(self, normalized_data, normalizer):
        try:
            data = np.squeeze(normalized_data) * (normalizer['max'] - normalizer['min']) + normalizer['mean']
            return data
        except KeyError:
            print("Error: invalid variable")